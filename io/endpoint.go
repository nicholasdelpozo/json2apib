package io

import "sort"

// EndPoint is a single api endpoint
type EndPoint struct {
	Raw            map[string]interface{}
	Title          string
	Description    string
	URL            string
	URLDescription string
	Method         string
	Properties     []Property
	ResAttributes  Property
	ReqAttributes  Property
}

func (e *EndPoint) sortAttributes() {
	sort.Sort(PropertyAlpha(e.ResAttributes.Children))
	sort.Sort(PropertyAlpha(e.ReqAttributes.Children))
}
