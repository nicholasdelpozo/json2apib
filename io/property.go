package io

import (
	"sort"
	"strconv"
	"strings"
)

// Property holds information about paramter used in action
type Property struct {
	Name        string
	Description string
	Type        string
	ItemType    string
	Example     string
	Optional    bool
	Items       []Property
	Children    []Property
	Parent      *Property
	Required    bool
}

func getDefFromRef(reference string, definitions *map[string]interface{}) map[string]interface{} {

	// split string into parts
	refArray := strings.Split(reference, "/")

	// start with definition as first step
	refData := *definitions

	// step through chain to find correct definition
	// start at 2 to skip the "#" and "definition" elements
	for i := 2; i < len(refArray); i++ {
		refData = asMap(refData[refArray[i]])
	}

	return refData
}

func makeProperty(name string, data map[string]interface{}, definitions *map[string]interface{}, parent *Property) Property {

	p := Property{
		Children: make([]Property, 0, 25),
		Name:     name,
		Parent:   parent,
	}

	// if this is a reference, read data from the reference object instead of the supplied data
	if rawRef, isRef := data["$ref"]; isRef {
		data = getDefFromRef(asString(rawRef), definitions)
	}

	// description : string
	if raw, hasDescription := data["description"]; hasDescription {
		p.Description = asString(raw)
	}

	// optional : bool
	if raw, hasOptional := data["optional"]; hasOptional {
		p.Optional = asBool(raw)
	}

	// get type
	// (rename integer => number)
	// if no type, but has properties, make object
	if raw, hasType := data["type"]; hasType {
		types := asArray(raw)

		p.Type = types[0].(string)

		if p.Type == "integer" {
			p.Type = "number"
		}
	} else if _, hasProperties := data["properties"]; hasProperties {
		p.Type = "object"
	}

	// example : string
	if raw, hasExample := data["example"]; hasExample {
		switch raw.(type) {
		case float64:
			p.Example = strconv.Itoa(int(raw.(float64)))
		case string:
			p.Example = raw.(string)
		}
	}

	// if this parameter is an object, we need to recursively get its children
	if p.Type == "object" {
		// if it has properties
		if rawChildProps, hasChildProps := data["properties"]; hasChildProps {
			childProperties := asMap(rawChildProps)

			// if the parent was an array, we need to wrap the object with an inline property tag
			// + (array)
			//     + - (object)
			//         + ...
			if parent.Type == "array" {

				inlineObject := Property{
					Type:     "object",
					Children: make([]Property, 0, 25),
				}

				// add inline object as child to paramter
				p.Children = append(p.Children, inlineObject)

				// recursively collate the properties for this object child
				p.Children[0].collateProperties(&childProperties, definitions)

				// sort the properties that were just added
				sort.Sort(PropertyAlpha(p.Children[0].Children))

			} else {

				// recursively collate the properties for the child
				p.collateProperties(&childProperties, definitions)

				// sort the properties that were just added
				sort.Sort(PropertyAlpha(p.Children))
			}
		}
	}

	// if the parameter is an array, we need to get each of its child properties
	// + (array)
	if p.Type == "array" {
		p.addArrayItems(data, definitions)
	}

	return p
}

func (p *Property) addArrayItems(data map[string]interface{}, definitions *map[string]interface{}) {

	// if has items
	if rawItems, hasItems := data["items"]; hasItems {
		items := asMap(rawItems)

		// if has item type
		if rawItemTypes, hasItemType := items["type"]; hasItemType {
			itemTypes := asArray(rawItemTypes)
			itemType := asString(itemTypes[0])

			// check item type
			switch itemType {

			// if the type of item is OBJECT it could either be an actual object array
			// or it could be an array OBJECT ARRAY ENUMERATION (could be one of various types)
			case "object":

				// if object has ANYOF element, we treat it as an enum:
				// + - (array)
				//     + - (enum)
				//         + - ...
				if rawAnyOf, hasAnyOf := items["anyOf"]; hasAnyOf {

					enumChild := Property{
						Type:     "enum",
						Children: make([]Property, 0, 25),
					}

					anyOf := asArray(rawAnyOf)

					// now for each object in the enum we make a new element with the enum as parent
					for index := range anyOf {
						optionchild := makeProperty("", asMap(anyOf[index]), definitions, &enumChild)
						enumChild.Children = append(enumChild.Children, optionchild)
					}

					// finally, add the enum child to the property children
					p.Children = append(p.Children, enumChild)
				}

				// if obejct has PROPERTIES lement, we treat it as an an inline object
				// + - (array)
				//     + - (object)
				//         + - ...
				if rawChildProps, hasChildProps := items["properties"]; hasChildProps {
					childProps := asMap(rawChildProps)

					objectChild := Property{
						Type:     "object",
						Children: make([]Property, 0, 25),
					}

					//add object child to paramter
					p.Children = append(p.Children, objectChild)

					// recursively collate the properties for the child
					p.Children[0].collateProperties(&childProps, definitions)

					// sort the properties that were just added
					sort.Sort(PropertyAlpha(p.Children[0].Children))
				}

			// if the array items are an array, we add them recursively
			case "array":

				// declare the array as array of specific type
				// give it an array type
				p.ItemType = "array"

				if _, hasChildItems := items["items"]; hasChildItems {

					arrayChild := Property{
						Type:     "array",
						Children: make([]Property, 0, 25),
					}

					arrayChild.addArrayItems(items, definitions)

					p.Children = append(p.Children, arrayChild)
				}

			// if the item type is not an obejct, we treat it as a primitive array
			default:

				// declare the array as array of specific type
				// give it an array type
				p.ItemType = itemType

				// get example from items
				if raw, hasExample := items["example"]; hasExample {
					switch raw.(type) {
					case float64:
						p.Example = strconv.Itoa(int(raw.(float64)))
					case string:
						p.Example = raw.(string)
					}
				}

				// description : string
				if raw, hasDescription := items["description"]; hasDescription {
					p.Description = asString(raw)
				}
			}
		}
	}
}

// recursively collate all properties as children of a parent node
func (p *Property) collateProperties(properties *map[string]interface{}, definitions *map[string]interface{}) {
	for name, data := range *properties {
		child := makeProperty(name, asMap(data), definitions, p)
		p.Children = append(p.Children, child)
	}
}
