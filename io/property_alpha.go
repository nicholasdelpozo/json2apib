package io

// PropertyAlpha is an alpha sorter for Proprties
type PropertyAlpha []Property

// sort methods for PropertyAlpha
func (e PropertyAlpha) Len() int           { return len(e) }
func (e PropertyAlpha) Swap(i, j int)      { e[i], e[j] = e[j], e[i] }
func (e PropertyAlpha) Less(i, j int) bool { return e[i].Name < e[j].Name }
