package io_test

import (
	. "apsdsm.com/json2apib/io"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Parser", func() {

	It("extracts the endpoint titles from a json file", func() {

		in := "../fixtures/a_banners.json"

		parsed := Parse(in)

		Expect(len(parsed.EndPoints)).To(Equal(6))

		endPointExpectations := []struct {
			title string
		}{
			{"Kamihime - A Banners Gacha"},
			{"Kamihime - A Banners Mypage"},
			{"Kamihime - A Banners Quest"},
			{"Kamihime - A Banners Shop"},
			{"Kamihime - A Banners Story Event"},
			{"Kamihime - A Banners Treasure Event"},
		}

		for i, e := range endPointExpectations {
			Expect(parsed.EndPoints[i].Title).To(Equal(e.title))
		}
	})

	It("extracts endpoint description from a json file", func() {
		in := "../fixtures/a_banners.json"

		parsed := Parse(in)

		description := "ガチャのバナー情報<br /><br /> ga_000で使用することを想定<br /> アカウントごとに割引画像などを表示する必要があるのでa_bannersとしております。<br />"

		Expect(parsed.EndPoints[0].Description).To(Equal(description))
	})

	It("extracts url data from json file", func() {
		in := "../fixtures/a_banners.json"

		parsed := Parse(in)

		Expect(len(parsed.EndPoints)).To(Equal(6))

		endPointExpectations := []struct {
			method, url, desc string
		}{
			{"GET", "/a_banners/gacha", "開催中のガチャバナーの一覧を取得する<br />"},
			{"GET", "/a_banners/mypage", "top/my_001/my_ev_bannar/menuで使用することを想定<br />"},
			{"GET", "/a_banners/quest", "クエストのバナーの一覧を取得する<br /> q_001で使用することを想定<br />"},
			{"GET", "/a_banners/shop", "ショップのバナーの一覧を取得する<br /> sh_001で使用することを想定<br />"},
			{"GET", "/a_banners/story_event?page=1&per_page=10", "ライブラリのストーリーイベントタブに表示するバナーの一覧を取得する<br /> no_002_bで使用することを想定<br />"},
			{"GET", "/a_banners/treasure_event?page=1&per_page=10", "トレジャー交換のイベントタブに表示するバナーの一覧を取得する<br /> sh_004_cで使用することを想定<br />"},
		}

		for i, e := range endPointExpectations {
			Expect(parsed.EndPoints[i].Method).To(Equal(e.method))
			Expect(parsed.EndPoints[i].URL).To(Equal(e.url))
			Expect(parsed.EndPoints[i].URLDescription).To(Equal(e.desc))
		}
	})

	It("extracts definitions from json file", func() {
		in := "../fixtures/a_banners.json"

		parsed := Parse(in)

		Expect(len(parsed.Definitions)).To(Equal(91))
	})

	It("builds response attributes", func() {
		in := "../fixtures/a_banners.json"

		parsed := Parse(in)

		endpoint := parsed.EndPoints[0]

		// there should be 2 children for the first end point response
		Expect(len(endpoint.ResAttributes.Children)).To(Equal(2))

		// expect parameter 1 to be array
		resAttr1 := endpoint.ResAttributes.Children[0]
		Expect(resAttr1.Name).To(Equal("data"))
		Expect(resAttr1.Type).To(Equal("array"))
		Expect(len(resAttr1.Children)).To(Equal(1))

		// expect attr1->child to be object
		resAttr1Child := resAttr1.Children[0]
		Expect(resAttr1Child.Type).To(Equal("object"))
		Expect(len(resAttr1Child.Children)).To(Equal(5))

		// expect attr1->child->attr1 to be banner_id
		resAttr1ChildAttr1 := resAttr1Child.Children[0]
		Expect(resAttr1ChildAttr1.Name).To(Equal("banner_id"))
		Expect(resAttr1ChildAttr1.Type).To(Equal("number"))
		Expect(resAttr1ChildAttr1.Example).To(Equal("1"))

		// expect attr1->child->attr1 to be description
		resAttr1ChildAttr2 := resAttr1Child.Children[1]
		Expect(resAttr1ChildAttr2.Name).To(Equal("description"))
		Expect(resAttr1ChildAttr2.Type).To(Equal("string"))
		Expect(resAttr1ChildAttr2.Example).To(Equal("Sレア以上のウェポンを必ずひとつゲット！"))

		// expect paramter 2 to be max_record_count
		resAttr2 := endpoint.ResAttributes.Children[1]
		Expect(resAttr2.Name).To(Equal("max_record_count"))
		Expect(resAttr2.Description).To(Equal("最大件数"))
		Expect(resAttr2.Type).To(Equal("number"))
		Expect(resAttr2.Example).To(Equal("1"))
	})

	It("builds anyof objects", func() {
		in := "../fixtures/a_battles.json"

		parsed := Parse(in)

		// the 12th endpoint should be `Kamihime - A Battle`
		battleAbility := parsed.EndPoints[11]
		Expect(battleAbility.Title).To(Equal("Kamihime - Battle Ability"))

		// the first child res attribute should be scenario
		scenario := battleAbility.ResAttributes.Children[0]
		Expect(scenario.Name).To(Equal("scenario"))

		// it should be an array
		Expect(scenario.Type).To(Equal("array"))

		// the child of this array should  be an enum
		enum := scenario.Children[0]
		Expect(enum.Type).To(Equal("enum"))

		// the enum should have 16 children
		Expect(len(enum.Children)).To(Equal(16))

		// each of those should be an object
		for i := range enum.Children {
			Expect(enum.Children[i].Type).To(Equal("object"))
		}
	})

	It("builds array of primitives", func() {
		in := "../fixtures/a_battles.json"

		parsed := Parse(in)

		// the 8th endpoint should be `Kamihime - A Battles Enemy Effect Resource Id List`
		battleAbility := parsed.EndPoints[7]
		Expect(battleAbility.Title).To(Equal("Kamihime - A Battles Enemy Effect Resource Id List"))

		// the first child res attribute should be ability
		ability := battleAbility.ResAttributes.Children[0]
		Expect(ability.Name).To(Equal("ability"))

		// it should be an array of numbers
		Expect(ability.Type).To(Equal("array"))
		Expect(ability.ItemType).To(Equal("number"))

		// the example should be 1
		Expect(ability.Example).To(Equal("1"))
	})

	It("builds array of arrays", func() {
		in := "../fixtures/a_battles.json"

		parsed := Parse(in)

		// the 8th endpoint should be `Kamihime - A Battles Enemy Effect Resource Id List`
		battleAbility := parsed.EndPoints[12]
		Expect(battleAbility.Title).To(Equal("Kamihime - Battle Attack"))

		// the 2nd child res attribute should be status
		status := battleAbility.ResAttributes.Children[1]
		Expect(status.Name).To(Equal("status"))

		// the 1st child of status should be abilityTurns
		abilityTurns := status.Children[0]
		Expect(abilityTurns.Name).To(Equal("ability_turns"))

		// it should be an array of arrays
		Expect(abilityTurns.Type).To(Equal("array"))
		Expect(abilityTurns.ItemType).To(Equal("array"))

		// the child of that array should be an array of numbers
		child := abilityTurns.Children[0]
		Expect(child.Type).To(Equal("array"))
		Expect(child.ItemType).To(Equal("number"))
		Expect(child.Description).To(Equal("cooldown left in turn numbers"))
	})

	It("assumes that property with child properties but no type is an object", func() {
		in := "../fixtures/a_battles.json"

		parsed := Parse(in)

		// the 13th endpoint should be `Kamihime - Battle Attack`
		battleAbility := parsed.EndPoints[12]
		Expect(battleAbility.Title).To(Equal("Kamihime - Battle Attack"))

		// the 2nd child res attribute should be status (it has no type)
		status := battleAbility.ResAttributes.Children[1]
		Expect(status.Name).To(Equal("status"))

		// it should be an object (even without type)
		Expect(status.Type).To(Equal("object"))
	})

	It("reads request properties", func() {
		in := "../fixtures/a_battles.json"

		parsed := Parse(in)

		// the 2nd endpoint should be `Kamihime - A Battle`
		battle := parsed.EndPoints[2]
		Expect(battle.Title).To(Equal("Kamihime - A Battle"))

		// the end point should have 3 request attributes
		Expect(len(battle.ReqAttributes.Children)).To(Equal(3))

		// the first attrbute should be a_party_id
		aPartyId := battle.ReqAttributes.Children[0]
		Expect(aPartyId.Name).To(Equal("a_party_id"))

		// it should be a number
		Expect(aPartyId.Type).To(Equal("number"))

		// it should be required
		Expect(aPartyId.Required).To(Equal(true))
	})
})
