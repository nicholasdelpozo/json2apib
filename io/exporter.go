package io

import (
	"bytes"
	"io/ioutil"
)

// ExportBuffer will dump the contents of a buffer to the specified location
func ExportBuffer(buffer *bytes.Buffer, path string) {
	_ = ioutil.WriteFile(path, buffer.Bytes(), 0644)
}
