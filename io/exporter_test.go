package io_test

import (
	"path/filepath"

	. "apsdsm.com/json2apib/io"

	"bytes"

	"io/ioutil"
	"os"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Exporter", func() {

	It("dumps a buffer to a file", func() {

		file, err := filepath.Abs("../fixtures/__test_output__")

		os.Remove(file)

		var buffer bytes.Buffer

		buffer.WriteString("foobarbaz")

		ExportBuffer(&buffer, file)

		in, err := ioutil.ReadFile(file)

		Expect(err).To(BeNil())
		Expect(string(in)).To(Equal("foobarbaz"))

	})
})
