package io

// Structure holds the overall api structure
type Structure struct {
	EndPoints   []EndPoint
	Definitions map[string]interface{}
}

// NewStructure makes a new empty Structure
func NewStructure() *Structure {
	s := Structure{}
	s.EndPoints = make([]EndPoint, 0, 25)
	s.Definitions = make(map[string]interface{})

	return &s
}

// addEndPoint adds a new EndPoint to the Structure
func (s *Structure) addEndPoint(rawData interface{}) {

	data := asMap(rawData)

	e := EndPoint{
		Raw: data,
	}

	// init properties
	e.Properties = make([]Property, 0, 25)

	// add title if exists
	if title, hasTitle := data["title"]; hasTitle {
		e.Title = title.(string)
	}

	// add description if exists
	if description, hasDescription := data["description"]; hasDescription {
		e.Description = description.(string)
	}

	// get reference to link
	links := data["links"].([]interface{})
	link := links[0].(map[string]interface{})

	// get URL and Method
	e.URL = link["href"].(string)
	e.Method = link["method"].(string)
	e.URLDescription = link["description"].(string)

	// allocate response attributes slice
	e.ResAttributes = Property{
		Type:     "root",
		Children: make([]Property, 0, 25),
	}

	// allocate reqest attributes slice
	e.ReqAttributes = Property{
		Type:     "root",
		Children: make([]Property, 0, 25),
	}

	s.EndPoints = append(s.EndPoints, e)
}

// addDefinition adds a new Definition to the definition map in the structure
func (s *Structure) addDefinition(name string, data interface{}) {
	s.Definitions[name] = data
}
