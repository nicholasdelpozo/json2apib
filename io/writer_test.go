package io_test

import (
	. "apsdsm.com/json2apib/io"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bytes"
)

var _ = Describe("Writer", func() {
	It("writes the endpoint title", func() {
		var buffer bytes.Buffer

		endpoint := EndPoint{
			Title: "Foo Title",
			URL:   "/foo/bar/baz",
		}

		WriteTitle(&endpoint, &buffer)

		Expect(buffer.String()).To(Equal("## Foo Title [/foo/bar/baz]\n\n"))
	})

	It("writes the endpoint description", func() {
		var buffer bytes.Buffer

		endpoint := EndPoint{
			Description: "foo bar baz!!",
		}

		WriteDescription(&endpoint, &buffer)

		Expect(buffer.String()).To(Equal("foo bar baz!!\n\n"))
	})

	It("writes the endpoint action", func() {
		var buffer bytes.Buffer

		endpoint := EndPoint{
			URLDescription: "Foo Action<br />",
			Method:         "GET",
		}

		WriteAction(&endpoint, &buffer)

		Expect(buffer.String()).To(Equal("### Foo Action [GET]\n\n"))
	})

	It("writes single response attribute", func() {
		var buffer bytes.Buffer

		endpoint := EndPoint{
			ResAttributes: Property{
				Type: "root",
			},
		}

		endpoint.ResAttributes.Children = []Property{
			Property{
				Name:        "foo",
				Type:        "number",
				Example:     "1",
				Description: "foo desc",
			},
		}

		WriteResAttributes(&endpoint, &buffer)

		res := "+ Response 200 (application/json)\n"
		res += "    + Attributes\n"
		res += "        + `foo` : `1` (number) - foo desc\n"

		Expect(buffer.String()).To(Equal(res))
	})

	It("writes attribute that contains array of object", func() {
		var buffer bytes.Buffer

		// endpoint with root node
		endpoint := EndPoint{
			ResAttributes: Property{
				Type: "root",
			},
		}

		// array node as first child of root node
		rootNode := &endpoint.ResAttributes
		rootNode.Children = []Property{
			Property{
				Name: "foo",
				Type: "array",
			},
		}

		// object node as first child of array node
		arrayNode := &rootNode.Children[0]
		arrayNode.Children = []Property{
			Property{
				Type: "object",
			},
		}

		// data node as first child of object node
		objectNode := &arrayNode.Children[0]
		objectNode.Children = []Property{
			Property{
				Name:        "bar",
				Type:        "number",
				Example:     "1",
				Description: "bar desc",
			},
		}

		WriteResAttributes(&endpoint, &buffer)

		res := "+ Response 200 (application/json)\n"
		res += "    + Attributes\n"
		res += "        + `foo` (array)\n"
		res += "            + - (object)\n"
		res += "                + `bar` : `1` (number) - bar desc\n"

		Expect(buffer.String()).To(Equal(res))

	})

	It("writes named objects", func() {
		// endpoint with root node
		endpoint := EndPoint{
			ResAttributes: Property{
				Type: "root",
			},
		}

		// array node as first child of root node
		rootNode := &endpoint.ResAttributes
		rootNode.Children = []Property{
			Property{
				Name: "foo",
				Type: "object",
			},
		}

		// data as first child of object node
		objectNode := &rootNode.Children[0]
		objectNode.Children = []Property{
			Property{
				Name:        "bar",
				Type:        "number",
				Example:     "1",
				Description: "bar desc",
			},
		}

		var buffer bytes.Buffer

		WriteResAttributes(&endpoint, &buffer)

		res := "+ Response 200 (application/json)\n"
		res += "    + Attributes\n"
		res += "        + `foo` (object)\n"
		res += "            + `bar` : `1` (number) - bar desc\n"

		Expect(buffer.String()).To(Equal(res))
	})

	It("writes enums of objects", func() {
		// endpoint with root node
		endpoint := EndPoint{
			ResAttributes: Property{
				Type: "root",
			},
		}

		// array node as first child of root node
		rootNode := &endpoint.ResAttributes
		rootNode.Children = []Property{
			Property{
				Name: "scenario",
				Type: "array",
			},
		}

		// enum as first child of array node
		arrayNode := &rootNode.Children[0]
		arrayNode.Children = []Property{
			Property{Type: "enum"},
		}

		// objects as children of enum node
		enumNode := &arrayNode.Children[0]
		enumNode.Children = []Property{
			Property{Type: "object"},
			Property{Type: "object"},
		}

		// first enum object concrete object
		enumNode.Children[0].Children = []Property{
			Property{Type: "number", Name: "foo", Example: "1", Description: "foo desc"},
		}

		// second enum child concrete object
		enumNode.Children[1].Children = []Property{
			Property{Type: "number", Name: "bar", Example: "2", Description: "bar desc"},
		}

		var buffer bytes.Buffer

		WriteResAttributes(&endpoint, &buffer)

		res := "+ Response 200 (application/json)\n"
		res += "    + Attributes\n"
		res += "        + `scenario` (array)\n"
		res += "            + - (enum)\n"
		res += "                + - (object)\n"
		res += "                    + `foo` : `1` (number) - foo desc\n"
		res += "                + - (object)\n"
		res += "                    + `bar` : `2` (number) - bar desc\n"

		Expect(buffer.String()).To(Equal(res))
	})

	It("writes single request attribute", func() {
		var buffer bytes.Buffer

		endpoint := EndPoint{
			ReqAttributes: Property{
				Type: "root",
			},
		}

		endpoint.ReqAttributes.Children = []Property{
			Property{
				Name:        "foo",
				Type:        "number",
				Example:     "1",
				Description: "foo desc",
				Required:    true,
			},
		}

		WriteReqAttributes(&endpoint, &buffer)

		res := "+ Request (application/json)\n"
		res += "    + Attributes\n"
		res += "        + `foo` : `1` (number, required) - foo desc\n"

		Expect(buffer.String()).To(Equal(res))
	})
})
