package io

// EndPointAlpha is an alpha sorter for EndPoints
type EndPointAlpha []EndPoint

// sort methods for EndPointAlpha
func (e EndPointAlpha) Len() int           { return len(e) }
func (e EndPointAlpha) Swap(i, j int)      { e[i], e[j] = e[j], e[i] }
func (e EndPointAlpha) Less(i, j int) bool { return e[i].Title < e[j].Title }
