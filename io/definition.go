package io

// Description is a data object that was made only to hold object descriptions
type Definition struct {
	Raw map[string]interface{}
}
