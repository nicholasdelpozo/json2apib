package io

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"
	"sort"
)

// Parse will parse json input and create a structure object
func Parse(in string) *Structure {

	// read file into buffer
	abs, err := filepath.Abs(in)

	if err != nil {
		panic("could not make absolute path from input")
	}

	buffer, err := ioutil.ReadFile(abs)

	if err != nil {
		panic("could not read file")
	}

	// generic interface to hold imported data
	var rawSource interface{}
	json.Unmarshal(buffer, &rawSource)

	// put source into a map
	var source map[string]interface{}
	source = asMap(rawSource)

	// create api object to decorate
	api := NewStructure()

	// if has definitions
	if definitionsData, hasDefinitions := source["definitions"]; hasDefinitions {
		definitions := asMap(definitionsData)

		// for each definitionName, definitionData add as end point or definition
		for definitionName, definitionData := range definitions {

			// if is endpoint add as special data structure
			if isEndPoint(definitionData) {
				api.addEndPoint(definitionData)
			}

			// always save as definition
			api.addDefinition(definitionName, definitionData)
		}
	}

	// sort endpoints alphabetically
	sort.Sort(EndPointAlpha(api.EndPoints))

	// for each end point build its response attributes
	for i := range api.EndPoints {
		endPoint := &api.EndPoints[i]

		if rawProperties, hasProperties := api.EndPoints[i].Raw["properties"]; hasProperties {
			properties := asMap(rawProperties)

			// collate respones attribute properties
			endPoint.ResAttributes.collateProperties(&properties, &api.Definitions)

			// sort attributes alphabetically
			sort.Sort(PropertyAlpha(endPoint.ResAttributes.Children))
		}

		if rawLinks, hasLinks := endPoint.Raw["links"]; hasLinks {
			links := asArray(rawLinks)
			link := asMap(links[0])

			if rawSchema, hasSchema := link["schema"]; hasSchema {
				schema := asMap(rawSchema)

				if rawProperties, hasProperties := schema["properties"]; hasProperties {
					properties := asMap(rawProperties)

					// collate the request parameters
					endPoint.ReqAttributes.collateProperties(&properties, &api.Definitions)

					// sort the attributes alphabetically
					sort.Sort(PropertyAlpha(endPoint.ReqAttributes.Children))
				}

				if rawRequired, hasRequired := schema["required"]; hasRequired {

					requiredAttrs := asArray(rawRequired)

					for ri := range requiredAttrs {
						requiredAttr := asString(requiredAttrs[ri])

						for ci := range endPoint.ReqAttributes.Children {
							if endPoint.ReqAttributes.Children[ci].Name == requiredAttr {
								endPoint.ReqAttributes.Children[ci].Required = true
							}
						}
					}
				}
			}
		}

	}
	return api
}

// if element has links, and length of links is > 0, then treat as end point
func isEndPoint(check interface{}) bool {

	checkMap := asMap(check)

	if raw, hasLinks := checkMap["links"]; hasLinks {
		links := asArray(raw)

		if len(links) > 0 {

			link := asMap(links[0])

			if _, hasHref := link["href"]; hasHref == false {
				return false
			}

			if _, hasMethod := link["method"]; hasMethod == false {
				return false
			}

			return true
		}
	}

	return false
}

// return interface as map
func asMap(data interface{}) map[string]interface{} {
	return data.(map[string]interface{})
}

// return interface as bool
func asBool(data interface{}) bool {
	return data.(bool)
}

// return interface as string
func asString(data interface{}) string {
	return data.(string)
}

// return interface as array of interface
func asArray(data interface{}) []interface{} {
	return data.([]interface{})
}
