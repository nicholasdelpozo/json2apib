package io_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestIO(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "io Suite")
}
