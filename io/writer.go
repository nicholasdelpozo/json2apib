package io

import (
	"bytes"
	"fmt"
	"strings"
)

// WriteTitle will add the title of a endpoint as md title
// e.g., ## New End Point [/foo/bar/baz]
func WriteTitle(endpoint *EndPoint, buffer *bytes.Buffer) {
	title := fmt.Sprintf("## %s [%s]\n\n", endpoint.Title, endpoint.URL)
	buffer.WriteString(title)
}

// WriteDescription will add the description of the endpoint as text
func WriteDescription(endpoint *EndPoint, buffer *bytes.Buffer) {
	description := fmt.Sprintf("%s\n\n", endpoint.Description)
	buffer.WriteString(description)
}

// WriteAction will add the endpoint action as md title
func WriteAction(endpoint *EndPoint, buffer *bytes.Buffer) {
	action := fmt.Sprintf("### %s [%s]\n\n", endpoint.URLDescription, endpoint.Method)
	action = strings.Replace(action, "<br />", "", -1)
	buffer.WriteString(action)
}

// WriteResAttributes will add the response attributes as md list
func WriteResAttributes(endpoint *EndPoint, buffer *bytes.Buffer) {
	buffer.WriteString("+ Response 200 (application/json)\n")
	buffer.WriteString("    + Attributes\n")

	unfoldProperties(&endpoint.ResAttributes, buffer, 2)
}

// WriteReqAttributes will add the request attributes as md list
func WriteReqAttributes(EndPoint *EndPoint, buffer *bytes.Buffer) {
	buffer.WriteString("+ Request (application/json)\n")
	buffer.WriteString("    + Attributes\n")

	unfoldProperties(&EndPoint.ReqAttributes, buffer, 2)
}

// recursively draw properties to the buffer
func unfoldProperties(property *Property, buffer *bytes.Buffer, depth int) {

	// default tab space
	tab := "    "

	for index := range property.Children {

		child := &property.Children[index]

		attribute := ""
		leftMargin := ""

		for i := 0; i < depth; i++ {
			leftMargin += tab
		}

		switch child.Type {

		case "array":

			attribute = leftMargin + "+ "

			if child.Name != "" {
				attribute += fmt.Sprintf("`%s` ", child.Name)
			}

			if child.Example != "" {
				attribute += fmt.Sprintf(": `%s` ", child.Example)
			}

			if child.ItemType != "" && child.Required {
				attribute += fmt.Sprintf("(array[%s], required) ", child.ItemType)
			} else if child.ItemType != "" {
				attribute += fmt.Sprintf("(array[%s]) ", child.ItemType)
			} else {
				attribute += "(array) "
			}

			if child.Description != "" {
				attribute += fmt.Sprintf("- %s", child.Description)
			}

		case "object":

			if child.Name != "" {
				attribute = leftMargin + fmt.Sprintf("+ `%s` ", child.Name)
			} else {
				attribute = leftMargin + "+ - "
			}

			if child.Required {
				attribute += "(object, required)"
			} else {
				attribute += "(object)"
			}

		case "enum":

			if child.Required {
				attribute = leftMargin + "+ - (enum, required)"
			} else {
				attribute = leftMargin + "+ - (enum)"
			}

		default:

			attribute = leftMargin + fmt.Sprintf("+ `%s` ", child.Name)

			if child.Example != "" {
				attribute += fmt.Sprintf(": `%s` ", child.Example)
			}

			if child.Required {
				attribute += fmt.Sprintf("(%s, required) ", child.Type)
			} else {
				attribute += fmt.Sprintf("(%s) ", child.Type)
			}

			if child.Description != "" {
				attribute += fmt.Sprintf("- %s", child.Description)
			}
		}

		attribute = strings.TrimRight(attribute, " ")
		attribute += "\n"
		buffer.WriteString(attribute)

		if t := child.Type; t == "array" || t == "object" || t == "enum" {
			unfoldProperties(child, buffer, depth+1)
		}

	}
}
