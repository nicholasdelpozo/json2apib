package main

import (
	"bytes"
	"fmt"
	"os"

	"apsdsm.com/json2apib/io"
)

func main() {

	if len(os.Args) < 3 {
		printUsage()
		os.Exit(0)
	}

	in := os.Args[1]
	out := os.Args[2]

	structure := io.Parse(in)

	var buffer bytes.Buffer

	for _, e := range structure.EndPoints {
		io.WriteTitle(&e, &buffer)
		io.WriteDescription(&e, &buffer)
		io.WriteAction(&e, &buffer)

		if len(e.ReqAttributes.Children) > 0 {
			io.WriteReqAttributes(&e, &buffer)
			buffer.WriteString("\n")
		}

		io.WriteResAttributes(&e, &buffer)
		buffer.WriteString("\n\n")
	}

	io.ExportBuffer(&buffer, out)

	os.Exit(0)
}

func printUsage() {
	fmt.Println("usage: io IN_FILE OUT_FILE")
}
